#!/bin/bash

# A simple guessing game

# Get a random number < 100
target=$(($RANDOM % 100))
# echo ${target}
# Initialize the user's guess
guess=

until [[ $guess -eq $target ]]; do
    read -p "Take a guess: " guess
    if [[ $guess -lt $target ]]; then
        echo "Higuer!"i
    elif [[ $guess -gt $target ]]; then
        echo "Lower!"i
    else
        echo "You found it!"
    fi
done

exit 0

#!/bin/bash
# this script if a test of conditional comparation in bash

echo -e "\e[34m***************************************************************\e[0m"
echo -e "\e[34mComparations in Shell script .....!\e[0m"
echo -e "\e[34m***************************************************************\e[0m\n"

export TIME_SP_HMM=`date "+%H%M"`
export temp_window=1700
#   Check Hours a minutes split
# if [ ${TIME_SP_HMM} < ${temp_window} ]; then
[[ ${TIME_SP_HMM} < ${temp_window} ]]
if [ $? = 0 ]; then

    echo -e "\e[34m***************************************************************\e[0m"
    echo -e "\e[34mCondicion is TRUE .....!\e[0m"
    echo -e "\e[34m***************************************************************\e[0m\n"
    exit 0
else
    echo -e "\e[31m***************************************************************\e[0m"
    echo -e "\e[31mCondición is False!!!!.\e[0m"
    echo -e "\e[31m***************************************************************\e[0m\n"
    exit 154
fi

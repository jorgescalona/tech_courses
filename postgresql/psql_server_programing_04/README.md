# Module 4 Advanced Server Programing

**types of Triggers in Postgres (When or Where):**

CREATE TRIGGER creates a new trigger. The trigger will be associated with the
specified table, view, or foreign table and will execute the specified function
function_name when certain operations are performed on that table.

![Triger type When](../static/images/trigger_when_00.png "Trigger When types")

![Triger type Where](../static/images/trigger_where_00.png "Trigger Where types")

![Triger types](../static/images/trigger_types_00.png "Trigger Types types")


* No trigger on SELECT statement
* Multiple triigers fires in alphabetical order
* Single trigger supports multiple actions

### Triggers like audit tool

Currently, four types of triggers exist in SQL Server, and the first two are
the most commonly used:

* DDL triggers (auditing CREATE, ALTER, DROP and several system defined stored
procedures which perform DDL operations) are often used for administrative
tasks like regulating and auditing database operations

* DML triggers – our focus will be on this type of triggers as they provide
auditing of data changes. This type of audit triggers fires upon DML statements
(like INSERT, UPDATE and DELETE) as well on stored procedures that execute
DML-like operations

* CLR triggers are a special type of auditing triggers which enables trigger
technique to be used directly within .NET languages, and they support SQL Server
2008 and above

* Logon triggers fire when a SQL Server LOGON event occurs. It can be used to
track successful authentication and control and audit server sessions

![m4 Summary](../static/images/m4_summary.png "m4 summary")

**note:** audit also can be design with RULES instead trigger.

### RULE

Allows user to define and alternative action to be performed on SELECT, INSERT,
UPDATE or DELETE operations in db.

![Rules](../static/images/m4_rules_over_triggers.png "rules over triggers")

### Interesting links

[Create Triggers](https://www.postgresql.org/docs/11/sql-createtrigger.html)
[triggers like auditing tool](https://solutioncenter.apexsql.com/auditing-triggers-in-sql-server-databases/)

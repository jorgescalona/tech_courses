﻿CREATE TABLE employee (
	empname text,
	salary integer
);

SELECT * FROM employee;


CREATE FUNCTION emp_stamp() RETURNS trigger AS $emp_stamp$
	BEGIN
		-- Check that empname and salary are given
		IF NEW.empname IS NULL THEN
			RAISE EXCEPTION 'Employee Name Colum cannot be null';
		END IF;
		IF NEW.salary IS NULL THEN
			RAISE EXCEPTION '% cannot have null salary', NEW.empname;
		END IF;

		-- Who works for us when she must pay for it?
		IF NEW.salary < 0 THEN
			RAISE EXCEPTION '% cannot have a negative salary', NEW.empname;
		END IF;

		RETURN NEW;
		
	END;
$emp_stamp$ LANGUAGE plpgsql;


CREATE TRIGGER emp_stamp BEFORE INSERT OR UPDATE ON Employee 
	FOR EACH ROW EXECUTE PROCEDURE emp_stamp();

--- test the trigger action

INSERT INTO employee (empname, salary)
VALUES ('Pinal', 500);

-- No employee Name
INSERT INTO employee (salary)
VALUES (100);

-- Null salary
INSERT INTO employee (empname, salary)
VALUES ('Jorge', null);

-- No salary
INSERT INTO employee (empname)
VALUES ('Jorge');

-- Negative salary
INSERT INTO employee (empname, salary)
VALUES ('Jorge', -100);

-- correct values

INSERT INTO employee (empname, salary)
VALUES ('Jorge',100);

SELECT * FROM employee;

--------------------------------   video demo: Data auditing with triggers
-- Audit with registry table

DROP TABLE employee;

CREATE TABLE Employee (
	empname text NOT NULL,
	salary integer
);

CREATE TABLE Employee_audit (
	operation char(1) NOT NULL,
	stamp timestamp NOT NULL,
	userid text NOT NULL,
	empname text NOT NULL,
	salary integer
);

SELECT * FROM Employee_audit;

CREATE OR REPLACE FUNCTION process_emp_audit() RETURNS TRIGGER AS $emp_audit$
	BEGIN
		IF (TG_OP = DELETE) THEN
			INSERT INTO Employee_audit SELECT 'D', now(), user, OLD.*;
			RETURN OLD;
		ELSIF (TG_OP = UPDATE) THEN
			INSERT INTO Employee_audit SELECT 'U', now(), user, OLD.*;
			RETURN OLD;
		ELSIF (TG_OP = INSERT) THEN
			INSERT INTO Employee_audit SELECT 'I', now(), user, OLD.*;
			RETURN OLD;
		END IF;
		RETURN NULL; -- return is ignored since this is an AFTER trigger
	END;
$emp_audit$ LANGUAGE plpgsql;


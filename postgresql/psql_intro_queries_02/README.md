# Intro to SQL Queries

### Integridad de datos

* optimiza los espacios de almacenamiento, el uso de la memoria y el CPU
* Se optimiza haciendo la selección correcta del tipo de datos

### Comandos psql

~~~
\dt
~~~

**Lista las tablas de la bd**

~~~
\d+ nom_tabla
~~~

**Lista la tabla con las propiedadaes de cada columna**

~~~
INSERT INTO Employee(ID, EmployeeName, Age, Birthdate, IsCurrentEmployee) VALUES (2, 'Juan', 36, '15/4/1983');
INSERT INTO Employee(ID, EmployeeName, Age, Birthdate, IsCurrentEmployee) VALUES (2, 'Juan', 36, '15/4/1983', B'1');
INSERT INTO Employee(ID, EmployeeName, Age, Birthdate, IsCurrentEmployee) VALUES (1, 'Maria', 38, '15/4/1981', B'0');
INSERT INTO Employee(ID, EmployeeName, Age, Birthdate, IsCurrentEmployee) VALUES (3, 'Jhon', 39, '15/4/1980', B'1');

ALTER TABLE employee ALTER COLUMN IsCurrentEmployee SET NOT NULL;
ALTER TABLE employee ALTER COLUMN IsCurrentEmployee SET DEFAULT B'1';
ALTER TABLE Customer ALTER COLUMN Zipcode SET DATA TYPE CHARACTER VARYING(12);
~~~

algunos ejemplos de **insert** y de modificar columna con **ALTER TABLE**

* Schemas
* Constrains
* Primary Key

![Primary Key](../static/images/pk_properties.png "Primary Key")

* Foreign Key

![Foraign Key](../static/images/fk_properties.png "Foraign Key")

**nota:** tanto la pk como la fk son de suma importancia para la integridad de los
datos en la BD, solo con ellas se pueden crear restricciones sobre DELETE por
ejemplo o hacer reglas para la misma como sería el CASCADE DELETE.

**Godd practice before delete:**

Antes de eliminar filas de la BD es **muy** buena práctica ejecutar el SELECT
relativo al DELETE y verificar que el resultado de la Query es el requerido, ya
luego podemos ejecutar el DELETE con toda certeza:

~~~
SELECT *
FROM actornew
WHERE "FilmCount" <= 30
ORDER BY actor_id;

SELECT *
FROM actornew
WHERE "actor_id" > 40 AND "FilmCount" <= 30
ORDER BY actor_id;

DELETE
FROM actornew
WHERE "actor_id" > 40 AND "FilmCount" <= 30;
~~~

**Summary:**

![Summary](../static/images/summary.png "Summary")

﻿-- query create employe table with pk id in new schema_2

CREATE TABLE schema_2.employee
(
  id integer PRIMARY KEY,
  employeename character varying(256),
  age integer,
  birthdate date,
  iscurrentemployee bit(1) NOT NULL DEFAULT B'1'::"bit"
)
WITH (
  OIDS=FALSE
);
ALTER TABLE schema_2.employee
  OWNER TO jorge;

-- query create EquipmentEmploye table with pk id
-- and fk employee_id  in new schema_2

CREATE TABLE schema_2.EquipmentEmployee
(
  id integer PRIMARY KEY,
  employee_id integer,
  equipment character varying(256),
  CONSTRAINT "Fkconst" FOREIGN KEY (employee_id)
      REFERENCES schema_2.employee ("id") MATCH FULL

 )
WITH (
  OIDS=FALSE
);
ALTER TABLE schema_2.employee
  OWNER TO jorge;

-- Insert records in schema_2.Employee table

INSERT INTO schema_2.Employee(ID, EmployeeName, Age, Birthdate, IsCurrentEmployee) VALUES (2, 'Juan', 36, '15/4/1983', B'1');
INSERT INTO schema_2.Employee(ID, EmployeeName, Age, Birthdate, IsCurrentEmployee) VALUES (1, 'Maria', 38, '15/4/1981', B'0');
INSERT INTO schema_2.Employee(ID, EmployeeName, Age, Birthdate, IsCurrentEmployee) VALUES (3, 'Jhon', 39, '15/4/1980', B'1');
INSERT INTO schema_2.Employee(ID, EmployeeName, Age, Birthdate, IsCurrentEmployee) VALUES (4, 'Juan', 36, '15/4/1983', B'1');

-- Insert records in schema_2.EquipmentEmployee table

INSERT INTO schema_2.EquipmentEmployee(ID, Employee_id, equipment) VALUES (1, 1, 'Laptop');
INSERT INTO schema_2.EquipmentEmployee(ID, Employee_id, equipment) VALUES (2, 1, 'Desk');
INSERT INTO schema_2.EquipmentEmployee(ID, Employee_id, equipment) VALUES (3, 3, 'Pencil');
INSERT INTO schema_2.EquipmentEmployee(ID, Employee_id, equipment) VALUES (4, 4, 'Truck');

-- queries

SELECT * FROM schema_2.equipmentemployee;
SELECT * FROM schema_2.employee;

-- q join

SELECT ep.employeename, eq.equipment 
FROM schema_2.employee ep INNER JOIN schema_2.equipmentemployee eq
ON eq.Employee_id = ep.id;


--  COPY and INSERT rows in tables

CREATE TABLE public.ActorNew
(
  actor_id integer NOT NULL,
  first_name character varying(45) NOT NULL,
  last_name character varying(45) NOT NULL,
  last_update timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT actornew_pkey PRIMARY KEY (actor_id)
)

-- Copy all rows from another table 
INSERT INTO public.ActorNew (actor_id, first_name, last_name,last_update) SELECT * FROM public.actor;

SELECT * FROM actornew limit 10;

SELECT a.actor_id, count(title) FilmCount
FROM Actor a
INNER JOIN Film_Actor fa ON a.actor_id = fa.actor_id
INNER JOIN Film f ON fa.film_id = f.film_id
GROUP BY a.actor_id
ORDER BY a.actor_id;


-- ADD filmcount row

ALTER TABLE public.actornew ADD COLUMN "FilmCount" integer;

-- update filmcount per actor and change last update to time now

UPDATE public.actornew
SET "FilmCount" = 
(SELECT count(title) FilmCount
FROM Actor a
INNER JOIN Film_Actor fa ON a.actor_id = fa.actor_id
INNER JOIN Film f ON fa.film_id = f.film_id
WHERE a.actor_id = actornew.actor_id
GROUP BY a.actor_id),
last_update = now();

-- DELETE sentences
﻿-- Remove rows with actor_id > 40
SELECT *
FROM actornew
WHERE "actor_id" > 40
ORDER BY actor_id;


-- Keep rows with filmcount > 30
-- Remove rows with filmcount <= 30
SELECT *
FROM actornew
WHERE "FilmCount" <= 30
ORDER BY actor_id;

SELECT *
FROM actornew
WHERE "actor_id" > 40 AND "FilmCount" <= 30
ORDER BY actor_id;

DELETE
FROM actornew
WHERE "actor_id" > 40 AND "FilmCount" <= 30;


  

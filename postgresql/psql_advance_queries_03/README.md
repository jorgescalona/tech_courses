# Module 3 Advance Queries

![Agenda](../static/images/agenda_course_iii.png "Agenda Course Module 3")

### Operations

* Functions:  Permiten ejecutar operaciones con sentencias sql y retornan un valor
simple.

![Math Opers and Functions](../static/images/math_operators_and_function_iii.png)

### Mathematical Operators

![Math Opers](../static/images/math_operators_iii.png "Math Operators")

### Mathematical Functions
![Math Functions](../static/images/math_functions_iii.png "Math Functions")

### Stirngs Func
![String Functions](../static/images/str_oper_and_func_iii.png "String Functions")
![String Functions ii](../static/images/str_oper_and_func_iii_00.png "String Functions ii")

### Date & DateTime Func

Las operaciones con fecha son de las más comunes es importante resaltar el uso
de **epoch**. Es un instante de tiempo elegido como el origen de una era particular.
**Epoch** es un punto de referencia desde el cual se empieza a medir el tiempo.
Entonces todos los calculos de fecha se haran con ese referente.

![Date Functions](../static/images/date_time_opr_iii_00.png "Date Functions")
![Date Functions ii](../static/images/date_time_func_iii_00.png "Date Functions ii")

### Aggregate Functions
![Agregate Functions](../static/images/agregate_func_iii_00.png "Aggregate Functions")

### Transaction

Las Transiciones agrupan multiples pasos en una operación **Todo o nada**.
Practicamente son scripts sql que ejecutan una serie de tareas.
![Transaction](../static/images/transaction_sql_iii_00.png "Transaction")

### enlaces de interés

* [Fuctionas & Opers](https://www.postgresql.org/docs/9.1/functions.html "Func & Oper")
* [Date func & Opers](https://www.postgresql.org/docs/8.1/functions-datetime.html "Date & datetime functions and opers")
* [Transaction documentation](https://www.postgresql.org/docs/8.3/tutorial-transactions.html "Transactions")

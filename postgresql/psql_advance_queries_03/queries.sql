﻿SELECT * FROM film
WHERE release_year = 2006
AND rental_duration = 6;

SELECT * FROM film
WHERE release_year = 2006
OR rental_duration = 6;

SELECT * FROM film
WHERE release_year = 2006
AND rental_duration != 6;

SELECT * FROM film
WHERE release_year = 2006
AND NOT rental_duration = 6;

SELECT * FROM film
WHERE rental_duration >= 2 AND rental_duration <= 6;

SELECT * FROM film
WHERE length=160 OR (rental_duration >= 2 AND rental_duration <= 6);

-- while converting length of movie from minute to hour

SELECT film_id, title, length, (length/60.0) length_in_hour,
	round((length/60.0),2) length_in_hour_round
FROM film;

-- where the length of the movie is over 2 hours

SELECT film_id, title, length, (length/60.0) length_in_hour,
	round((length/60.0),2) length_in_hour_round
FROM film
WHERE (length/60.0) >2;

-- while converting rental_date to nearest higher integer

SELECT film_id, title, rental_rate,
ceiling(rental_rate) new_rental_rate 
FROM film;

-- Advance Mathematical Function 
-- Fin Area of the store withh of diameter 500 feets
/*
Area of circle = PI * (radius)^2
Area of circle = PI * (diameter/2)^2
*/

SELECT PI()*(500/2)^2 AS AreaOfCircle;

-- Date and datetime Functions

SELECT (return_date - rental_date) rented_days, EXTRACT(epoch FROM return_date - rental_date)/3600
FROM rental
WHERE EXTRACT(epoch FROM return_date - rental_date)/3600 > 3;


-- Transictions

SELECT * FROM accounts;

BEGIN;
	UPDATE accounts
	SET amount = amount - 100
	WHERE name = 'Beth';

SAVEPOINT FirstUpdate;
	SELECT * FROM accounts;

	-- Incorrect Update
	UPDATE accounts
	SET amount = amount + 100
	WHERE name = 'Pinal';

	SELECT * FROM accounts;

ROLLBACK TO FirstUpdate;

	SELECT * FROM accounts;

	UPDATE accounts
	SET amount = amount + 100
	WHERE name = 'Troy';
	
	SELECT * FROM accounts;

COMMIT;


#  Course 05 Index Tuning and Perfomance

![module summary](../static/images/05_index_performance_sumary_00.png)

## EXPLAIN

its a sql sentence that permit execute a calulate something interesting parameters
about performance of queries.

This parameters are calculated in base of:
~~~
COST = (disk pages read * seq_page_cost ) + (rows * cpu_tuple_cost)
~~~
A few of this parameters can be read in **pg_class** table, by example:

~~~
SELECT relpages AS "Disk Page Read", reltuples AS "Rows Scanned"
FROM pg_class
WHERE relname = 'film'
~~~

## Indexes

### Interesting links:

[EXPLAIN](https://www.postgresql.org/docs/9.1/sql-explain.html)

@jorgemustaine 2020

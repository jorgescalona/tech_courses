﻿-- Display Courses and students in it
SELECT st.StudentName, cl.ClassName FROM StudentClass sc
INNER JOIN Students st ON st.StudentID = sc.StudentID
INNER JOIN Classes cl ON cl.classid = sc.classid;

-- Display student that not in courses
SELECT st.StudentName FROM Students st
LEFT JOIN StudentClass sc ON st.studentid = sc.studentid
LEFT JOIN classes cl ON cl.classid = sc.classid
WHERE cl.classname IS Null;

-- Display courses emptys
SELECT cl.classname FROM studentclass sc
RIGHT JOIN classes cl ON cl.classid = sc.classid
WHERE sc.classid IS Null;








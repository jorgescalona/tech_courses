### PostgreSQL 
## fitrst course
# from pluralsight


* listar para first name > 1 es decir para ocurrencias superiores a 1:

~~~
SELECT first_name, count(first_name) FROM actor WHERE actor_id > 99
GROUP BY first_name HAVING COUNT(first_name) > 1;
~~~

### JOIN and InnerJoin

**InnerJoin:** es la intersección de datos entre dos tablas, entendiendo cada tabla
de la bd como un conjunto de datos el **InnerJoin** es la intersección de los dos
conjuntos.

![InnerJoin](../static/images/ij_00.png "InnerJoin")

### Left Outer JOIN

**LeftOuterJoin:** Devuelve todas las filas de la tabla izquierda que  no coinciden
con las filas de la tabla derecha, si las filas no coinciden devuelve NULL.

![LeftOuterJoin](../static/images/loj_00.png "LeftJoin")

~~~
SELECT st.StudentName FROM Students st LEFT JOIN StudentClass sc ON st.studentid = sc.studentid LEFT JOIN classes cl ON cl.classid = sc.classid WHERE cl.classname IS Null;

SELECT st.studentname FROM students st LEFT JOIN studentclass sc ON st.studentid = sc.studentid WHERE sc.classid IS Null;
~~~

### Rigth Outer JOIN

Devuelve todas las filas de la tabla Derecha que no coinciden en la tabla
izquierda. Si no existen columnas coincidentes en la tabla izquierda retorna
Null.

![RigthOuterJoin](../static/images/roj_00.png "Rigth Join")

~~~
SELECT cl.classname FROM studentclass sc RIGHT JOIN classes cl ON cl.classid = sc.classid WHERE sc.classid IS Null;
~~~

### Cross JOIN

Es un JOIN cartesiano que no necesita una condición para hacer la intersección
de tablas. Retorna un conjunto de registros que son multiplos del número de
registros de ambas tablas.

![CrossJoin](../static/images/cross_join.png "Cross Join")

~~~
SELECT st.StudentName, cl.ClassName FROM classes cl CROSS JOIN students st;
~~~

![CrossJoin 2](../static/images/cross_join_01.png "Cross Join 2")

### Cross JOIN

Combina **left outer join** y **rigth outer join**. Retorna las filas de cada tabla,
incluso aquellas que retornan nulo.

![FullJoin](../static/images/full_join.png "Full Join")


### SUMARY

![Sumario](../static/images/sumary.png "Summary")

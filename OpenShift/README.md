# OpenShift

its a PaaS service of RedHat.

![OCP](static/images/OS-ICON-02.png)

can be deploy apps from an existing container image that you have built outside
the **OCP** or one that is supplied by a third party.

If you have a source code you can have a OpenShift build the image from instructions
provided by a **Dockerfile**.

### Deploying Applications

* From an existing container image hosted on an image registry located outside the
OpenShift cluster.
* From an existing container image that has been imported into the image registry
running inside the OpenShift cluster.
* From application source code in a Git repository hosting service. The application
source code would be built into an image inside OpenShift, using an S2I builder.
* From image source code in a Git repository hosting service. The image source
code would be built into an image inside OpenShift using instructions provided
in a Dockerfile .
* From application source code pushed into OpenShift from a local filesystem
using the command-line oc client. The application source code would be built
into an image inside OpenShift using an S2I builder.
* From image source code pushed into OpenShift from a local filesystem using the
command-line oc client. The image source code would be built into an image
inside OpenShift using instructions provided in a Dockerfile .

**Command to deploy container to configure OpenShift Origin:**

~~~
docker run -ti -p 6622:22 -p 6443:443 -p 8443:8443 -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/openshift:/var/lib/openshift ubuntu:latest
~~~

### Interesting Link

* [OpenShift Official Site](https://www.openshift.com/)
* [Install with docker](https://github.com/openshift/origin/blob/master/examples/sample-app/container-setup.md)
* [s2i Install](https://github.com/openshift/source-to-image)

[![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)
# Courses

This repo contain info about learning process about, inspired by the courses of
the platform [PluralSigth](https://www.pluralsight.com "PluRalSigth"),
If you find any content interesting, I recommend you visit the mentioned
services and subscribe to their training plans.

This repo contain info about learning process about, inspired by the courses of
the platform [udemy](https://www.udemy.com/),
If you find any content interesting, I recommend you visit the mentioned
services and subscribe to their training plans.

This repo contain info about learning process about, inspired by the courses of
the platform [google](https://googlecourses.qwiklabs.com/),
If you find any content interesting, I recommend you visit the mentioned
services and subscribe to their training plans.

@jorgemustaine 2019-2021


# Udemy Course terraform certification


### Interesting links:

* [terraform course on udemy](https://www.udemy.com/course/hashicorp-terraform)
* [repo around the course](https://github.com/terraformdpac/practica-terraform)
* [terraform registry](https://registry.terraform.io/)

@jorgemustaine 2023

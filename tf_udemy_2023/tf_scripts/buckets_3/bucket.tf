provider "aws" {
  region = var.region  # Replace with your desired region
}

resource "aws_s3_bucket" "uterraformcourse_03" {
  count  = 6
  bucket = "uterraformcourse-${random_string.sufijo[count.index].result}"
  tags = {
    Name        = "My bucket"
    Environment = "Dev"
    Course      = "uterraformcourse"
    User        = "jorgescalona"
    
  }
}

resource "random_string" "sufijo" {
  count   = 6
  length  = 8
  special = false
  upper   = false
  numeric = false
}
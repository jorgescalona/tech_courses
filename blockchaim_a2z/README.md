<img src="static/images/blockchain.webp" alt="blockchain" width="70"/>

# Blockchain


This site contain key info about **Blockchain A-Z™: Learn How To Build Your First Blockchain**

from udemy platform [courselink](https://www.udemy.com/course/build-your-blockchain-az/)

### Interesting links:

* [Where to get the Materials](https://www.superdatascience.com/pages/blockchain)
* [hash tool](https://tools.superdatascience.com/blockchain/hash)

@jorgemustaine 2022

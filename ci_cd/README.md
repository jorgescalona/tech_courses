[![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)
# CICD

This repo contain info about learning process about Continuous Integration and
Continuous Delivery, inspired by the course of the platform [PluralSigth](https://www.pluralsight.com "PluRalSigth"),
If you find any content interesting, I recommend you visit the mentioned
services and subscribe to their training plans.

[SaaS](https://es.wikipedia.org/wiki/Software_como_servicio "Software Como Servicio")

#  Teams

* Product Management (Proyect managers, líderes de proyectos, funcionales, casos,
unitest, test funcional)
* Software Development (devs, test of form, unitest)
* Integration & Operations (mezclan u/o integran el trabajo de los distintos
equipos de desarrollo y hacen el despliegue hacia lo distintos ambientes de
prueba y/o producción **DEPLOY**).
* Quality Assurance (QA, testing, test desempeño, security test, proponen REF)

![Equipos](static/images/teams.png)

Definir iteraciones cortas para la interacción entre equipos es la clave,
reuniones periodicas y comunicación permanente y fluida. (herramientas de gestión
como CRM, projectsManagement, git, son claves para aumentar el knowHow del team)


El equipo de integración se en encarga de la administración del sistema de control
de versiones, gobernanza del mismo y mezcla de los distintos branches que lo
componen. **Merge, Build & Deploy**

![Equipo de Integración](static/images/integration_work.png)
![Tareas de Integración](static/images/task_integration_team.png "Tasks Integration team")

Luego el equipo de operaciones recibe el release a ser desplegado DEPLOY.

![Operations Team](static/images/deploy_process.png "Tasks Operation team")

La integración manual de errores es propensa a errores, cuando el equipo de
integración debe hacer repetidas tareas unión de dispositivos para lograr
prototipos funcionales, es hora de pensar en la **automatización de procesos** ,está
práctica requiere grandes cantidades de recursos:

* Muchas personas en la misma tarea. (devs y operations, proyect managers)
* Diferentes equipos y enviroments entre devs, tests and production.

# CI
**Ventajas:**

* Menos recursos en el proceso de integración.
* el historial queda registrado y disponible, facil de auditar. Mejora la
comunicación de todos los actores y el equipo.
* reducción de incidencias. (más calidad)
* Entregas más rápidas y menudas
* Reduce los costos de producción. (+en cloud)

![ci](static/images/ci_diagram.png "CI")

# CD Continous Delivery and Continous Deployment

Es la disciplina de desarrollo de software, desde donde el software es promovido
a producción (test u aceptación), en algún momento. Delivery y Deployment se
diferencia en que el primero se entrega el artefacto, mientras en el segundo se
despliega de manera automática.

**nota:** Los ambientes de deasarrollo, testing y producción son homogeneos y en su
totalidad iguales gracias a que el deployment de cada uno se hace mediante
scripts que garantizan su aislamiento. (yaml, dockerfiles, tf, etc)

Solo la cultura interna del **DevOps** garantiza la entrega constante de artefactos
funcionales, ya que hace sinergia entre los equipos de desarrollo y el de
operaciones.

**CI is mandatory to pass to CD over this premiss CI generated a Release and this
last must be versioned.**

![ci](static/images/ci_diagram_00.png "CI")
![delivery Summary](static/images/delivery_summary.png "Delivery Summary")

[![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)

#  Kubernetes fundamentals

**Overview:**

![Kubernetes](static/images/kubernetes_ico.png)

* What is?

![Kubernetes](static/images/k_what_is.png "What is kubernetes")

* Cluster
* Cli tool **Kubectl**
* Build Kubernetes cluster

### API

* RESTful API over HTTP using JSON


### Pods

* Desire State (State, Health)
* **Controllers** verify the state of our pods

### Services

Add percistence to the ephimeral kubernetes world, enruta y resuelve la red
interna y la comunicación entre **pods** como también repone los pods que por algún
motivo han sido destruidos. y hace **Load Balancing** 

### Volumes

Unidades de almacenamiento directamente accesible desde los PODS ofrecen
persistencia de datos.

### Architecture

![Arch 1](static/images/k_arch_00.png)
![Arch 2](static/images/k_arch_01.png)


## Kubernetes Lab Scenario

![Kubernetes Lab Scenario](static/images/k_lab_scenario_01.png "kubernetes lab")

## Etcd

is a distributed key-value store. In fact, etcd is the primary datastore of
Kubernetes; storing and replicating all Kubernetes cluster state. As a critical
component of a Kubernetes cluster having a reliable automated approach to its
configuration and management is imperative.

## Important notes

1. It is key to configure the docker daemon through json file to modify the
cgroups for systemd.
1. In the case that your display is on aws, the ami must be minimum t2.medium
2 CPUs.

## Interesting links

* [Kubernetes](https://kubernetes.io/es/)
* [Kubernetes in github](https://github.com/kubernetes/kubernetes)
* [etcd](https://coreos.com/blog/introducing-the-etcd-operator.html)
* [calico](https://docs.projectcalico.org/introduction/)
* [Config master and nodes for the course](https://www.centinosystems.com/blog/linux/updated-getting-started-with-installing-kubernetes/)
* [kubernetes for a beginner](https://www.youtube.com/watch?v=X48VuDVv0do)

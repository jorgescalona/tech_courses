# DIVE k8s 

That is a resource for documentation of learning process on GCP cloud platform.

First I try to complete the official first steps with GKE.

## Interesting Links:

* [kubernetes engine](https://cloud.google.com/kubernetes-engine "k8s engine")
* [Hello app on GKE](https://cloud.google.com/kubernetes-engine/docs/tutorials/hello-app "Quik Start with GKE")

    **note:** the command `gcloud auth configure-docker` will modify your conf
    docker file in: `~/.docker/config.json` add the folowing:

~~~
 {
    "credHelpers": {
    "gcr.io": "gcloud",
    "us.gcr.io": "gcloud",
    "eu.gcr.io": "gcloud",
    "asia.gcr.io": "gcloud",
    "staging-k8s.gcr.io": "gcloud",
    "marketplace.gcr.io": "gcloud"
 }
}
~~~


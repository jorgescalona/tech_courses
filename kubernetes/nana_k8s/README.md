# Kubernetes Tutorial for Beginners.
## by nana

### YAML Configuration File in kubernetes


**Overview:**

1. The 3 parts of configuration file.
   * matadata.
   * specifications.
        * Inside spec you can find replicas, selector and template, last one have
        their own metadata and spec, for pod config.
   ![config yaml](../static/images/k8s_nana_yaml_config_00.png "Config yaml file")
   * status (automate creation by k8s and extrac that info from etcd bd).
2. Connecting Deployments to Service to Pods.
3. Demo.


**Stablish networks and connections: `[labels and selectors]`**

The labels are used in **metadata** and selectors in **spec** and services, with that info kubernetes
can connect similar components between them.

![labels and selectors](../static/images/k8s_nana_yaml_config_01.png "labels and selectors")

**Use case with mongoDb and mongo-express:**

we will create two apps and connect mongo-express to mongo db with configmap and secret files refrenced from deployment.yaml

![mongo use case](../static/images/k8s_nana_mongo_00.png "use case for mongodb")

### Interesting links:

* [k8s for beginners video 4hours](https://www.youtube.com/watch?v=X48VuDVv0do)
* [Commands repo](https://gitlab.com/nanuchi/youtube-tutorial-series/-/blob/master/basic-kubectl-commands/cli-commands.md)
* [Youtube tutorial series by nana](https://gitlab.com/nanuchi/youtube-tutorial-series)

@jorgemustaine 2023


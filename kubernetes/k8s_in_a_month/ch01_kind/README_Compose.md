[![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)

# Docker Compose Lab

For build an environment to make your labs in the folder ch01, I stored three
key files:  Dockerfile, docker-compose.yaml and setup.sh, only you should be run
the command: `docker-compose up -d`  and ready to play.

After you can modify the **setup.sh** file to provisioning the container.

@jorgemustaine 2021

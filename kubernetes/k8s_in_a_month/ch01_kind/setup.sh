#  This script is create by jorgescalona @jorgemustaine escajorge@gmail.com for
#  deploy a k8s lab docker based for learning process
#  LICENSE by Creative Commons CC-BY 4.0
#  International Public License
#!/bin/bash
apt-get update && apt-get install -y vim wget curl aptitude &&\
apt-get install -y apt-transport-https ca-certificates gnupg lsb-release &&\
aptitude update &&\
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg &&\
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null &&\

# Here only install docker cli into container because I use the docker engine from HOST
# You can see that in docker-compose file where make a volume for this  patch
aptitude update && aptitude install -y docker-ce-cli &&\

#  Now for install k3d its for use rancher/k3s in docker
#  https://github.com/rancher/k3d
wget -q -O - https://raw.githubusercontent.com/rancher/k3d/main/install.sh | bash &&\

curl -Lo ./kubectl https://storage.googleapis.com/kubernetes-release/release/v1.18.8/bin/linux/amd64/kubectl &&\
chmod +x ./kubectl && mv ./kubectl /usr/local/bin/kubectl &&\

#  Install kubectl and other k8s grass like:
#  https://www.techrepublic.com/article/how-to-quickly-install-kubernetes-on-ubuntu/
#
#curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add &&\
#echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" >> /etc/apt/sources.list &&\
#aptitude update && aptitude install -y kubelet kubeadm kubectl kubernetes-cni &&\

# Add aliases for work in my container
echo "alias d='docker'" >> /root/.bashrc &&\
echo "alias k='kubectl'" >> /root/.bashrc &&\
echo "/swap.img none swap sw 0 0" >> /etc/fstab
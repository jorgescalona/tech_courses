# k8s in a month

It's a set of notes about my learning process with the book:
[Kubernetes, in a month of lunches](https://livebook.manning.com/book/learn-kubernetes-in-a-month-of-lunches/)

## Key Concepts:

**Cluster:** Is a set of individual servers which have all been configured with a
container runtime like docker.

![k8s cluster](../static/images/k8s_cluster_00.png "K8s cluster")


## Interesting links:

* [kiamol repo book example in github](https://github.com/sixeyed/kiamol)
* [Official kubernetes documentation](https://kubernetes.io/)
* [kubernetes for a beginner](https://www.youtube.com/watch?v=X48VuDVv0do)

## k8s frameworks

* [kops](https://kops.sigs.k8s.io/)
* [k3s](https://k3s.io/)

@jorgemustaine 2020

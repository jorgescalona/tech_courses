    5  k get nodes
   13  k get pods | grep hello-kiamol
   14  k --version
   15  k -v
   16  k -V
   17  k version
   18  k run hello-kiamol --iamge=kiamol/ch02-hello-kiamol --restart=Never
   19  k run hello-kiamol --image=kiamol/ch02-hello-kiamol --restart=Never
   20  k get pods | grep hello-kiamol
   21  k get pods
   22  k describe pod hello-kiamol
   23  k describe pod hello-kiamol | less
   24  d image ls
   26  k get pods
   31  kubectl get pod hello-kiamol --output custom-columns=NAME:metadata.name,NODE_IP:status.hostIP,POD_IP:status.podIP
   32  kubectl get pod hello-kiamol -o jsonpath='{.status.containerStatuses[0].containerID}'
   36  docker container ls -q --filter label=io.kubernetes.pod.name=hello-kiamol
   37  docker container rm -f $(docker container ls -q --filter label=io.kubernetes.pod.name=hello-kiamol)
   39  docker container ls -q --filter label=io.kubernetes.pod.name=hello-kiamol
   41  kubectl port-forward pod/hello-kiamol 8080:80

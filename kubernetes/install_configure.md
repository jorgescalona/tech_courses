# Install & Configure Tips

### Components

**Kubernets:**

* [kubelet](https://kubernetes.io/docs/reference/command-line-tools-reference/kubelet/)
* [kubeadm](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm/)
* [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/)
* [container runtime docker](https://www.docker.com/get-started)

## Installation requirements

**Necessary ports:**

| Component          | Ports (tcp)   | Used by       |
| ---------          | -----------   | --------      |
| API                | 6443          | All           |
| etcd               | 2379 - 2380   | API/etcd      |
| Scheduler          | 10251         | Self          |
| Controller Manager | 10252         | Self          |
| Kubelet            | 10250         | Control Panel |
| Node Port          | 30000 - 32767 | All           |

## Build a cluster

![Kubernetes Build Cluster](static/images/k_build_cluster_00.png "kubernetes cluster")

## Kubernetes install RECIPE

~~~
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
~~~
~~~
sudo touch /etc/apt/sources.list.d/kubernetes.list && sudo chmod 777 /etc/apt/sources.list.d/kubernetes.list
~~~
~~~
sudo echo -e 'deb https://apt.kubernetes.io/ kubernetes-xenial main' >> /etc/apt/sources.list.d/kubernetes.list
~~~
~~~
sudo apt-get update && sudo apt-get install -y kubelet kubeadm kubectl
~~~

## Modify MEM parameters in /boot (debian based systems)

Verify that file in **/boot/config-X.X.X-XX-generic** and add:

~~~
CONFIG_RESOURCE_COUNTERS=y
CONFIG_MEMCG=y
CONFIG_MEMCG_SWAP=y
CONFIG_MEMCG_SWAP_ENABLED=y
CONFIG_MEMCG_KMEM=y
~~~

## kubernetes certificates

![Kubernetes Build Cluster](static/images/k_certificate_00.png "kubernetes certificates")

## Creating a master

* Download yaml manifests script calico to describe our pod network:

**in this describe security needed to deploy**
~~~
wget https://docs.projectcalico.org/v3.3/getting-started/kubernetes/installation/hosted/rbac-kdd.yaml
~~~
**others description configurations to pod networks**
~~~
wget https://docs.projectcalico.org/v3.3/getting-started/kubernetes/installation/hosted/kubernetes-datastore/calico-networking/1.7/calico.yaml
~~~

* Initialize and define CIDR Network (range of address)

~~~
kubeadm init --pod-network-cidr=192.168.0.0/16
~~~

### interesting links

* [Kubernetes doc getting started](https://kubernetes.io/docs/setup/)
* [Install minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)

// Golang script maked by jorgescalona for coursera GO course 2020
package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

type Name struct {
	fname string
	lname string
}

func first_char(s string) string {
	runes := []rune(s)
	return string(runes[0:20])
}

func main() {
	var fileName string
	nameSli := make([]Name, 0)
	var nameObj Name

	fmt.Print("Add file name: ")
	fmt.Scan(&fileName)
	file, err := os.Open(fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		seeker := strings.Split(scanner.Text(), " ")
		if len(seeker[0]) > 20 {
			seeker[0] = first_char(seeker[0])
		}
		if len(seeker[1]) > 20 {
			seeker[1] = first_char(seeker[1])
		}

		nameObj.fname, nameObj.lname = seeker[0], seeker[1]
		nameSli = append(nameSli, nameObj)
	}

	for _, v := range nameSli {
		fmt.Println(v.fname, v.lname)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

package main

import (
	"fmt"
	"sort"
	"strconv"
)

func main() {
	sArr := make([]int, 3)
	var input string

	fmt.Println("Input an integer (X to exit) :")

	for true {
		fmt.Scanln(&input)

		if input == "X" {
			break
		}

		aInput, err := strconv.Atoi(input)
		if err != nil {
			fmt.Println("Wrong input")
			continue
		}

		sArr = append(sArr, aInput)
		sort.Ints(sArr[:])

		fmt.Println(sArr)
		fmt.Println("Input an integer (X to exit) :")
	}
}

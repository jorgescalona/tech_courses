package main
// script go maked by jorgescalona for coursera GO course 2020
import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
)

func main() {
	// Create the data structure = map
	my_map := make(map[string]string)

	var name string
	var address string

	in := bufio.NewScanner(os.Stdin)
    // read or scan inputs
	fmt.Print("Add your name: ")
	in.Scan()
	name = in.Text()

	fmt.Print("Add your address: ")
	in.Scan()
	address = in.Text()

	my_map["name"] = name
	my_map["address"] = address

	jsonObj, err := json.Marshal(my_map)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(jsonObj))
}

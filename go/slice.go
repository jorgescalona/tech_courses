package main
// by jorgescalona for coursera golang course
import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	num_list := make([]int, 0, 3)

	var number string
	for {
		fmt.Print("Add a number please", ": ")
		_, err := fmt.Scan(&number)
		exitCode := strings.ToLower(number)
		if err != nil {
			fmt.Println(err)
			os.Exit(0)
		}
		if exitCode == "x" {
			fmt.Println("Exiting...")
			os.Exit(0)
		}

		conv_int_num, err := strconv.Atoi(number)
		num_list = append(num_list, conv_int_num)
		sort.Ints(num_list)
		fmt.Println(num_list)
	}
}

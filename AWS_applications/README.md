# AWS applications

Este curso abarca las características básicas de administración de instancias en
AWS. comienza con una explicación del servicio [Elastic beanstalk](https://en.wikipedia.org/wiki/AWS_Elastic_Beanstalk)
que es el *IaC* de está nube, con el mismo podemos con unos pocos clicks hacer
deploy de un ambiente para distintas tecnologías.


# BeansTalk App

### RDS env vars

![rds env vars](static/images/rds_env_vars.png "rds env vars")

### Load Balancer

Un balanceador de carga en AWS se encarga de multiplexar los user requests y
redireccionarlos a las intancias E2C que corresponda. Además autoescala los
recursos contra-demanda. Esto permite configuraciones tán granulares que permiten
por ejemplo crear nuevas **E2C** si la existente esta sobre solicitada, y de hacer
terminate de la misma cuando los requests sobre ella bajen.

Se puede configurar el reemplazo de instancias por lotes (baths).

![Load Balancer](static/images/load_balancer.png "AWS Load Balancer")

![BeansTalk summary](static/images/beans_talk_summary.png " BeansTalk Summary")

# AWS Lambda



### Enlaces de Interes

* [Documentación Oficial de AWS](https://aws.amazon.com/es/elasticbeanstalk/)
* [BeansTalk](https://aws.amazon.com/es/elasticbeanstalk/)
* [AWS Lambda](https://aws.amazon.com/es/lambda/)


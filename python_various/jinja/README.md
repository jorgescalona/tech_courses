# Jinja

Modern templating language for python.

## Template inheritance

Templates usually take advantage of inheritance, which includes a single base
template that defines the basic structure of all subsequent child templates. You
use the tags {% extends %} and {% block %} to implement inheritance.

### Interesting links

* [jinja site](https://jinja.palletsprojects.com/en/2.11.x/)
* [jinja realpython tutorial](https://realpython.com/primer-on-jinja-templating/)

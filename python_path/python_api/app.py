from flask import Flask, jsonify, request, render_template

app = Flask(__name__)

@app.route('/')  # make a request to root web structure
def home():
    return "Hello, world!"


app.run(port=5000)


from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from flask_jwt import JWT, jwt_required

from security import authenticate, identity

app = Flask(__name__)
app.secret_key = 'jose'
api = Api(app)

jwt = JWT(app, authenticate, identity)  # /auth

items = list()


class Item(Resource):
    """define an item in the store"""
    parser = reqparse.RequestParser()
    parser.add_argument(
        'price',
        type=float,
        required=True,
        help='This field cannot be left blank!'
    )

    @jwt_required()
    def get(self, name):
        """return items list"""
        item = next(filter(lambda x: x['name'] == name, items), None)
        return {'item': item}, 200 if item else 404

    def post(self, name):
        """create a new item or return error if item name is already exist"""
        if next(filter(lambda x: x['name'] == name, items), None) is not None:
            return {
                'message': "An item with name {} already exist".format(name)
            }, 400

        # obtain data from json playload
        data = Item.parser.parse_args()
        item = {
            'name': name,
            'price': data['price']
        }
        items.append(item)
        return item, 201

    def delete(self, name):
        """purge an exist item"""
        global items
        items = list(filter(lambda x: x['name'] != name, items))
        return {'message': 'Item "{}" deleted'.format(name)}

    def put(self, name):
        """update an item or create it!"""
        data = Item.parser.parse_args()

        item = next(filter(lambda x: x['name'] == name, items), None)
        if item is None:
            item = {'name': name, 'price': data['price']}
            items.append(item)
        else:
            item.update(data)
        return item


class ItemList(Resource):
    """Define a set of items from a store"""

    def get(self):
        """return a collection of items"""
        return {'items': items}


#  http://127.0.0.1:5000/item/<name>
api.add_resource(Item, '/item/<string:name>')
api.add_resource(ItemList, '/items')

app.run(port=5000, debug=True)

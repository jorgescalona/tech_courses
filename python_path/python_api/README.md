# Python APIs

Documentation about Python APIs course by udemy.

An API is a program that takes in some data and gives back some other data,
usually after processing it.

## Rest Verbs

|REST verb| syntax | complement |
|---------|--------|------------|
|  GET    | /item/chair/ |      |
|  POST   | /item/chair/ | with extra data |
|  PUT    | /item/chair/ | with extra data |
| DELETE  | /item/chair/ |      |



**For run javascript code in brave browser or chromium you [should run:](https://stackoverflow.com/questions/3102819/disable-same-origin-policy-in-chrome)**

~~~
brave-browser --disable-web-security --user-data-dir="[work_dir]"
~~~

## Python user authenticated 

We can authenticate an user with **json web token**, there is a lib for install
it:

`pip install Flask-JWT`

### Interesting links:

* [Code for Udemy Course](https://github.com/schoolofcode-me/rest-api-sections/tree/master/section3 "code course")
* [postman](https://www.postman.com/)
* [API REST Python RealPython](https://realpython.com/api-integration-in-python/)
* [Status codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes)
* [JSON placeholder](https://jsonplaceholder.typicode.com/)

@jorgemustaine 2021

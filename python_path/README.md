# Python inmersion


Here I'm recopile all information about the python essential concepts and
methodologies.

## String Concatenation:

If you want that join a few strings the best practice is use the **join** method because the last one use less memory for the process:

`''.join(str1+str2+str3..+strn)`

or:

~~~
complete_name = ['jhon', 'michael', 'doe']
str_complete_name = ' '.join(complete_name)
str_complete_name.split(' ')
~~~

## List comprehension Syntax:

`[expr(item) for item in iterable]`

Similary we can build data structure for set comprehensions or dict comprehensions:

~~~
>>> from math import factorial as fac
>>> s = {len(str(fac(x))) for x in range(20)}
~~~

## lambda functions
### lambda x, y: x + y

[lambda functions](https://realpython.com/python-lambda/ "Python lambda functions")


Example use lambda function to obtain a list with double of each element:

~~~
list(map(lambda x: x*2, list_name))
~~~


## Generator Functions

![generators](static/images/generators_yield_00.png "generators")

## Refactoring types:

![ref types](static/images/ref_types_00.png "ref types")
![ref types](static/images/ref_types_01.png "ref types")
![ref types](static/images/ref_types_03.png "ref types")

## Sorting dicts

![Sorting dicts](static/images/sorting_dicts_00.png "sorting dicts")

## Classmethods vs Sataticmethods:

![class vs static methods](static/images/classmethod_vs_staticmethod_00.png)

## Interesting links:

@jorgemustaine 2020

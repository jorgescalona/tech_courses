my_name = 'Jorge Mustaine'
my_age = '42'
my_height = '182'  # centimeters
my_weight = '90'  # kgs
my_eyes = 'Brown'
my_teeth = 'White'
my_hair = 'Black'

print(f"Let's talk about {my_name} ")
print(f"He's {my_height} centimeters tall.")
print(f"He's {my_weight} kgs heavy. ")
print('Actually that is not too heavy.')
print(f"He's got {my_eyes} eyes and {my_hair} hair.")
print(f"His teeth are usually {my_teeth} depending on the coffee.")

# this is a trick, try to get it exactly right
total = int(my_age) + int(my_height) + int(my_weight)
print(f"If i add {my_age}, {my_height}, and {my_weight} I get {total}")


print("How old are you?,", end=': ')
age = input()
print("How tall are you?,", end=': ')
height = input()
print("How much do you weigh?,", end=': ')
weight = input()

print(f"So, you're {age} old, {height} tall and {weight} heavy.")
print("So, you're {0} old, {1} tall and {2} heavy.".format(
    age, height, weight))

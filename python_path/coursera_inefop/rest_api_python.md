# REST API

It's the documentation for the python's specialization on Michigan University.

**Rest API:** Just mean a website that produces data intended for another computer program to consume

**URL form:**

~~~
{protocol}://{server}/{arguments}
~~~

The protocol part generally is: **http, https, ftp, between others**,  after the separator: **://**, finally the
arguments, generally the path or other important data.

**Query Parameters**

In a URL address can be a question mark, that's an indicator for a query:

~~~
https://events.umich.edu/list?filter=tags:Art,&range=2018-10-01
~~~

**Request to an API** is made by visiting a URL. Some API use a different part of the http protocol, call post mechanism


## interesting links:

* [request lib real python course](https://realpython.com/python-requests/ "Courese by realpython")
* [request for humans](https://requests.readthedocs.io/en/master/ 'Course about rquest')
* [json lib](https://docs.python.org/3.8/library/json.html 'Official docs')
* [request with caching](https://www.coursera.org/lecture/data-collection-processing-python/the-requests-with-caching-module-fTMx4 'Request with caching')

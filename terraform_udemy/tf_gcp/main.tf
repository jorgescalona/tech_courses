terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.5.0"
    }
  }

}
provider "google" {

  credentials = file(var.GCP_FILE_JSON)

  project = var.GCP_PROJECT_ID
  region  = var.GCP_REGION_ID
  zone    = var.GCP_ZONE_ID
}

resource "google_compute_network" "vpc_network" {
  name = "terraform-network"
}

resource "google_compute_instance" "vm_instance" {
  name         = "terraform-instance"
  machine_type = "f1-micro"

  boot_disk {
    initialize_params {
      image = "ubuntu-1804-bionic-v20210119a"
      //      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    network = google_compute_network.vpc_network.name
    access_config {
    }
  }

}
resource "google_compute_firewall" "http-server" {
  name    = "default-allow-http-ssh"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["80", "22"]
  }

  // Allow traffic from everywhere to instances with an http-server tag
  source_ranges = ["0.0.0.0/0"]
  //  target_tags   = ["http-server"]
}

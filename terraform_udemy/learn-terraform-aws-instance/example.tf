provider "aws" {
  profile = "jescalona"
  region  = "us-west-2"
}

resource "aws_instance" "example" {
  # ami           = "ami-0a741b782c2c8632d"
  ami           = "ami-0663daa9397d0beed"
  instance_type = "t2.micro"
}

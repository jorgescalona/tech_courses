# Terraform

This site it's for documentation about learning process of IaC with terraform.

### Outputs in tf

Terraform keeps attributes of all the resources you create. And you can access
to those information making reference to specific attribute:

`resorceType.resourceName.Attribute`

**for example:**

`aws_instance.example.public_ip`


We always can configurate a remote bakend for store the state and sensible data
for execute terraform, and after add a **backend.tf** file with the respective
configuration to that backend for example [**Consul**](https://www.consul.io/)

There is and example of backend.tf:

~~~
terraform {
  backend "consul" {
    address = "demo.consul.io"   # hostname of consul cluster
    path = "terraform/myproject"
  }
}
~~~

Or maybe on aws **S3:**

~~~
terraform {
  backend "s3" {
    address = "mybucket"
    key     = "terraform/myproject"
    region  = "us-west-1"
  }
}
~~~

## VPC (Virtual Private Network)

In AWS VPC insolates the instances on a network level. It's like your **own
private network** in the cloud.

* For smaller to medium setup, one VPC (per region) will be suitable for your
needs.

* An instance launched in one VPC can never communicate with an instance in an
other VPC using their **private IP addresses**. Those instances only can communicate
between each other with **public IP** (not recommended), It's better use link 2 VPCs
called **peering**.

![creating VPC and subnetting](static/images/creating_subnets_by_vpc_00.png "CreateVPCandSubnets")
![Subnets](static/images/masks_subnets_by_vpc_00.png "MaskForSubnets")

## EBS on AWS:

* [AWS EBS](https://aws.amazon.com/es/ebs/?ebs-whats-new.sort-by=item.additionalFields.postDateTime&ebs-whats-new.sort-order=desc)
* [EBS type of instances](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-optimized.html "type of ebs instances")
* [demo-9 course code example about EBS](https://github.com/wardviaene/terraform-course/tree/master/demo-9)

## Route53 on AWS:

* [Route53 on AWS](https://console.aws.amazon.com/route53/v2/home#Dashboard "route53")
* [demo-11 course code example](course_code_example/demo-11)

## AmI and Roles:

[AWS IAM roles](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles.html)
![IAM roles](static/images/iam_roles_aws_00.png "iam roles")

Roles can give users and services temporary access. The roles can be for instances
attached to EC2 instances. From that instance a user or services can obtain access
credentials. Using those credentials the user or service can assume the role.

**recipe to create IAM roles:**
![iam policies](static/images/iam_policies_01.png)

1. Create a group and attach the AWS managed Administrator Policy to it.
![create user and attach 2 group](static/images/create_group_and_attach_2_policy.png)
1. Create a user an attach it to a group.
![create user and attach 2 group](static/images/create_user_and_attach_2_group.png)
![iam policies](static/images/iam_policies.png)
 
* [demo-13 course code example IAM user creation](course_code_example/demo-13)
* [demo-14 course code example Roles creation](course_code_example/demo-14)

## AWS AutoScaling:
AWS Auto Scaling monitors your applications and automatically adjusts capacity
to maintain steady, predictable performance at the lowest possible cost.

* [AWS autoscaling](https://aws.amazon.com/es/autoscaling/)
* [demo-15 course code example Autoscaling](course_code_example/demo-15 "demo15")

## AWS ELB:
Elastic Load Balancing automatically distributes incoming application traffic
across multiple targets, such as Amazon EC2 instances, containers, IP addresses,
Lambda functions, and virtual appliances, but also can be a [SSL Terminator](https://avinetworks.com/glossary/ssl-termination/)


* [AWS autoscaling](https://aws.amazon.com/es/elasticloadbalancing "AWS ELB")
* [demo-16 course code example ELB](course_code_example/demo-16 "demo16 autoscaling ELB")

## AWS Elastic BeanStalk:
AWS Elastic Beanstalk is an easy-to-use service for deploying and scaling web
applications and services developed with Java, .NET, PHP, Node.js, Python, Ruby,
Go, and Docker on familiar servers such as Apache, Nginx, Passenger, and IIS.

You can simply upload your code and Elastic Beanstalk automatically handles the
deployment, from capacity provisioning, load balancing, auto-scaling to application
health monitoring. At the same time, you retain full control over the AWS
resources powering your application and can access the underlying resources at
any time.


* [AWS BeanStalk](https://aws.amazon.com/elasticbeanstalk/ "AWS Elastic BeanStalk")
* [demo-16 course code example ELB](course_code_example/demo-17 "demo17 BeanStalk")

## AWS Interpolation:
Embedded within strings in Terraform, whether you're using the Terraform syntax
or JSON syntax, you can interpolate other values. These interpolations are
wrapped in `${}`, such as `${var.foo}`.

The interpolation syntax is powerful and allows you to reference variables,
attributes of resources, call functions, etc. [Terraform Interpolation](https://www.terraform.io/docs/configuration-0-11/interpolation.html)

You can perform simple math in interpolations, allowing you to write expressions
such as `${count.index + 1}`. And you can also use conditionals to determine a
value based on some logic.

![interpolation](static/images/interpolation_00.png)
![interpolation](static/images/interpolation_01.png)

* [AWS expressions](https://www.terraform.io/docs/configuration/expressions.html "AWS expressions")
* [demo-18 course code example Interpolation and conditionals](course_code_example/demo-18 "demo18 interpolation & conditionals")

## Terraform project Structure:

* [demo-18b course code example tf project structure](course_code_example/demo-18b "demo18b project structure example")


## devops using terraform and AWS

![devops with tf and aws](static/images/devops_with_tf_00.png "devops with tf and aws")
![devops demo](static/images/devops_with_tf_01.png "devops demo lab")

* [packer_demo course code example tf project structure](course_code_example/packer-demo "packer_demo")

You should see too [packer_demo_ with_jenkins](course_code_example/jenkins-packer-demo "demo packer and jenkins")

# Modules in terraform

You can either use external [modules](https://www.terraform.io/docs/language/modules/syntax.html), or write modules yourself.
There are many libraries banks for differents modules for example you can find
on [github aws modules examples](https://github.com/terraform-aws-modules) maintained by the community.

[Module](https://github.com/in4it/terraform-modules) used on course.

## Interesting Links:

* [HashiCorp learning Site](https://learn.hashicorp.com/terraform)
* [site locate ec2 ubuntu instances by region](https://cloud-images.ubuntu.com/locator/ec2/)
* [github repo for udemy course](https://github.com/wardviaene/terraform-course)
* [Course Slides](http://assets.in4it.s3.amazonaws.com/public/udemy/learn-devops-terraform-dec2020.pdf)
* [terraform functions](https://www.terraform.io/docs/language/functions/index.html)
* [terraform modules](https://www.terraform.io/docs/language/modules/syntax.html)

## Reverse generation of tf files infrastructure

* [Reverse IaC from AWS](https://github.com/dtan4/terraforming)
* [Reverse IaC from GCP](https://github.com/GoogleCloudPlatform/terraformer)


# Terraform

this repo contain info about of learning process over [PluralSigth](https://www.pluralsight.com/) Platform

If you think you are interested in this material, I invite you to subscribe on
the site.

Here are only my personal notes about the course.

### IaC

Infraestructure as a code, is provisioning infrastructure througth software to
achieve consistent and predictable enviroments.

Describe the deploys trougth of configuration files in format like: Json, Yaml or
HariCorp format.

![Core Concepts](static/images/core_concepts.png "Core Concept")

### Benefits of IaC

* Automated deployment.
* Consitent enviroments.
* Repeatable process.
* Reusable components.
* Documented architectured.

**Premises:**

1. Infrestructure as a code isn't scary.
1. Manual processes are the enemy.

### Terraform Components

* terraform executable.
* terraform files. **(\*.tf)**
* terraform plugins
* terraform state files

**aws, create key pair from cli:**

~~~
aws ec2 create-key-pair --key-name MyKeyPair --query 'KeyMaterial' --output text > MyKeyPair.pem
~~~

### Terraform syntax

**Objects types in terraform:**

![Objects in tf](static/images/tf_syntax_objects_00.png "Objects syntax")

# Module 4
### Planning Updates

* **Terraform State**
    * JSON format
    * Mapping Resources and metadata
    * **Locking** is the way of the terraform protect determinate infrastructure to
    dont acept more changes.
    * Location. site where terraform store the state file:
        * Local: same working directory
        * Remote: AWS, Azure, NFS, Terraform Cloud.
    * Workspaces
* **Terraform Plan**
    * Inspect state
    * Dependency Graph
    * Management additions, Deletions and Updates
    * Parallel execution
    * Save the plan into **tf** file

**FIRST RULE OF TERRAFORM:  Make all changes in terraform**

### AWS VPC (Virtual Private Cloud)

Is the logic site in which you can launch AWS resources in a virtual network that
you define. You can control all aspects of the virtual network environment,
including selecting your own range of IP addresses, creating subnets and
configuring routing tables and network gateways.

![Aws VPC Sections](static/images/aws_vpc_sections_00.png "AWS VPC Sections")

# Module 6
### Functions in HashiCorp language

![Commons Functions](static/images/common_functions_in_tf_00.png "Commons functions")

# Module7
###  Using variables and functions

![Vault Managing Secrets](static/images/vault_managing_secrets.png "Vault managing Secrets")


### tf modules and tf register

Module is a mini terraform configuration.

![tf modules](static/images/tf_modules_00.png "Terraform modules")
![tf mod comp](static/images/tf_modules_01.png "Module Components")
![tf mod ex](static/images/tf_modules_02.png "Module Example")

# Final considerations:

1. Build infrastructure **automagically**.
1. Ensure consistent and repetable deployment.
1. Reuse existing configurations.  keywork **Modules**.
1. Make your job better or find a better job.

### Interesting Links:

* [Examples in github of instructor](https://github.com/ned1313/Getting-Started-Terraform "examples files")
* [AWS cli config](https://docs.aws.amazon.com/es_es/cli/latest/userguide/cli-chap-configure.html)
* [HashiCorp Syntax by Terraform](https://www.terraform.io/docs/configuration/index.html)
* [Terraform provisioners](https://www.terraform.io/docs/provisioners/index.html "tf provisioners")
* [HashiCorp Vault](https://www.vaultproject.io/ "Vault")
* [Vault tuto](https://www.terraform.io/docs/providers/vault/index.html)
* [Terraform Registry](https://registry.terraform.io/)
* [tf examples](https://github.com/hashicorp/terraform/tree/master/examples)

### Importat Notes

1. Is imperative add tfvars file extension to gitignore to prevent push it to
remote version control host.


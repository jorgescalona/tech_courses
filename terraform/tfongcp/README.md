# terraform for GCP

This is the result of the tuto on [terraform with GCP](https://learnk8s.io/terraform-gke)

### Interesting links:

* [Google provider terraform configuration](https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference)

@jorgemustaine 2021

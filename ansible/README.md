# Ansible
<p align="center">
  <img src="static/images/ansible_ico_00.png">
</p>

Is provisioning tool. Like an IT automation tool, can configure system, deploy
software and orchestrate more advance IT task such as continuous deployment or
zero downtime rolling updates. More about ansible [Here](https://docs.ansible.com/ansible/latest/index.html).

Ansible works by running commands over SSH. Is a decentralized tool. 

**Playbooks:** are just YAML files that use special terminology to infrom Ansible
about what it should do.

## Ansible connecting

![Ansible Connect](static/images/ansible_connect_00.png)

to list different connect sockets:

`ansible-doc -t connection -l`

## Interesting links:

* [ansible official documentation](https://docs.ansible.com/)
* [Vagrant Documentation](https://www.vagrantup.com/docs)
* [Vagrant boxes search](https://app.vagrantup.com/boxes/search)
* [course ansinble getting started github](https://github.com/g0t4/course-ansible-getting-started/tree/master/inventory)
* [Ansible for DevOps Examples](https://github.com/geerlingguy/ansible-for-devops)

@jorgemustaine 2020

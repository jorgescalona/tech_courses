# Getting Started with jenkins 2

its a learning metodology about **Jenkins** in the [PluralSigth](https://www.pluralsight.com/) Platform.

### Install

Exist a lot methods to install view [Here](https://jenkins.io/download/) i select docker way:

~~~
docker run -u root -d -p 8088:8080 -p 50000:50000 \
-v /local_path/jenkins_home:/var/jenkins_home \
-v /var/run/docker.sock:/var/run/docker.sock \
--entrypoint="apt-get update && apt-get -y install maven" \
jenkins/jenkins
~~~

its necesary into the container install **maven** or pass by entrypoint.

![anatomy of a build](static/images/anatomy_of_a_build.png "Anatomy of a build")

**Difference between job and build:**

* job its a process definition like a compile, testing or package
* the job is defined in a xml file (like config.xml in **jenkins_home/job/name/config.xml**)
* for general the job is one and builds one or various

![anatomy of a job](static/images/anatomy_of_a_job.png "Anatomy of a Job")

### Module 5 Building Continous Delivery Pipeline

![Continous Delivery](static/images/deploy_pipeline.png "Continous Delivery")

* A process that is very important is the backup and restore configuration of
the state of jenkins server, the same is like next:

    1. locate the file jenkins configuration in `.jenkins/` folder
    1. zip the same `tar -czf name_of_arch.tgz .jenkins/`
    1. unzip in target machine an ready to joy! **Happy Deploy!**

### interesting links

* [Gist of the Course](https://gist.github.com/g0t4/747cd20e8563aefc3eac444166983142)
* [repo with demo](https://github.com/g0t4/jenkins2-course-spring-boot)
* [maven](https://es.wikipedia.org/wiki/Maven)
* [Install for production](https://www.digitalocean.com/community/tutorials/how-to-install-jenkins-on-ubuntu-18-04)
* [Oficial Groovy Documentation](http://docs.groovy-lang.org/)
* [Groovy-string-interpolation](http://docs.groovy-lang.org/latest/html/documentation/index.html#_string_interpolation)
* [Groovy Scripts](https://www.guru99.com/groovy-tutorial.html)
* [Jenkins GitLab Plugin Documentation](https://github.com/jenkinsci/gitlab-plugin)

@jorgemustaine 2019

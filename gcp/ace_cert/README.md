# GCP Associate Cloud Engineer
### Google Cloud Certification from Udemy

### Static Ip Addresses

You can find an static Ip address of two forms. The first one It's with a load
balancer and the second one It's make a Ip reservation.

## Module 4 Optimizing costs and performance in GCP
### Finops and efficient costs

**Automatic Discounts:** for running VM instances of type N1, N2. Don't apply
for certain machine types (ex: E2, A2). Either apply for machines created by
App Engine Flex and DataFlow.

**Committed use discounts:** For workloads with predictable resource needs. The
same can be for one or three years. Up to 70% discount based on machine type
and GPUs. Aplly for GKE and GCE instances. Don't apply for App Engine Flex and
DataFlow.

`Compute engine/Virtual machines/Committed use discounts`

**Preemptible VM** you can use for **fault tolerance** app. save up to 80%. Some
restrictions are:

    1. Not always available.
    1. NO SLA and CANNOT be migrated to regular VMs.
    1. No automatic restart.

### Interesting links:

* [Course resources](https://github.com/in28minutes/course-material/blob/main/09-google-certified-associate-cloud-engineer/downloads.md)


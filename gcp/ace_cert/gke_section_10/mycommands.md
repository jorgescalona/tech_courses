# Owns Commands for this section 10

~~~
gcloud container clusters create test-std-cluster-jorgescalona --machine-type=n1-standard-2 --zone=us-east1-b --num-nodes=2 --max-nodes=10 --min-nodes=2 --labels=key1=value1,key2=value2
gcloud container clusters delete test-std-cluster-jorgescalona --zone=us-east1

# to scale number of nodes on cluster
gcloud container clusters resize test-std-cluster-jorgescalona --node-pool=default-pool --num-nodes=3 --zone=us-east1-b
~~~

This command configures kubectl to use the cluster you created:

`gcloud container clusters get-credentials hello-cluster`


### kubectl commands:

**deployments** management the version and release for a microservice.
The **replicaset** configure the numbers of pods for each miroservice.
One **service** expose your deployment to the rest of the world. Ensures that
the external world doesn't get impacted as pods go down and come up. There are
three types of services:

1. **ClusterIP:** Exposes Service on a cluster-internal IP.
1. **LoadBalancer:** Exposes Service externally using a cloud provider's load balancer.
1. **NodePort:** Exposes Service on each Node's IP at static port (the NodePort).

~~~
kubectl get nodes
kubectl create deployment hello-world-rest-api --image=in28min/hello-world-rest-api:0.0.1.RELEASE
kubectl get deployment
kubectl expose deployment hello-world-rest-api --type=LoadBalancer --port=8080
kubectl get service
curl EXTERMAL-IP:8080
curl EXTERMAL-IP:8080/hello-world

# to scale the number of replica by workloads for example:
kubectl scale deployment hello-world-rest-api --replicas=2

# for make a horizontal autoscaling foour eployment
kubectl autoscale deployment hello-world-rest-api --max=4 --cpu-percent=70

# for verify that autoscaling
kubectl get hpa

# for deploy a V2 of microservices
kubectl set image deployment m1 m1=m1:v2

# scale pods
kubectl scale deployment hello-world-rest-api --replicas=4

# create a service
kubectl expose deployment name --type=LoadBalancer --port=8
~~~

@jorgemustaine 2021

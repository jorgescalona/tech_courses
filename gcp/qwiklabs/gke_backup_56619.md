# GKE backup & restore

[gke backup qwiklab](https://www.cloudskillsboost.google/focuses/56619)

It's a service for backing up and restoring workloads in GKE clusters. It has
two principal components: 

1. Google Cloud API. as the control plane for the service.
1. GKE addon(the backup for GKE agent), must be enabled in each cluster for
which you wish to perform backup and restore operations.

[GKE bckup] Basic steps:

* Enable Backup for a GKE cluster. 
* Deploy a stateful application with a database on GKE.
* Plan and Backup GKE workloads.
* Restore Backup.

### Tasks or steps:

* Task 1. Enable Backup for GKE.
* Task 2. Create a backup plan.
* Task 3. Deploy WordPress with MySQL to the cluster.
* Task 4. Deploy the application.
* Task 5. Verify the deployed workload.
* Task 6. Create a backup.
* Task 7. Delete the application.
* Task 8. Plan a restore.
* Task 9. Restore a backup.

### Set up the environment

A GKE cluster and a static external IP address were provisioned as part of the
lab setup.

* Run the following commands to set the required environment variables:

```
echo "export ZONE=us-central1-a" >> ~/.bashrc
echo "export REGION=us-central1" >> ~/.bashrc
echo "export PROJECT_ID=`gcloud config get-value core/project`" >> ~/.bashrc
echo "export BACKUP_PLAN=my-backup-plan" >> ~/.bashrc
source ~/.bashrc
echo "export EXTERNAL_ADDRESS=$(gcloud compute addresses describe app-address --format='value(address)' --region $REGION)" >> ~/.bashrc
source ~/.bashrc
```

## Task 1. Enable Backup for GKE

1. Enable the Backup for GKE APIs:

`gcloud services enable gkebackup.googleapis.com`

You should see the following success message:

`Operation "operations/####.##-############-########-####-####-####-############" finished successfully.`

2. Enable Backup for GKE on an existing cluster. You can enable Backup when you
create a new cluster, but for this lab you will enable it on an existing cluster:

```
gcloud beta container clusters update lab-cluster \
--project=$PROJECT_ID  \
--update-addons=BackupRestore=ENABLED \
--zone=$ZONE
```

Your results should look like this:

```
Updating lab-cluster...done.
Updated [https://container.googleapis.com/v1beta1/projects/qwiklabs-gcp-##-############/zones/us-central1-a/clusters/lab-cluster].
To inspect the contents of your cluster, go to: https://console.cloud.google.com/kubernetes/workload_/gcloud/us-central1-a/lab-cluster?project=qwiklabs-gcp-##-############
```

3. Verify Backup for GKE is enabled on the cluster

```
gcloud beta container clusters describe lab-cluster \
--project=$PROJECT_ID  \
--zone=$ZONE | grep -A 1 gkeBackupAgentConfig:
```

## Task 2. Create a backup plan

1. Run the following backup plan:

```
gcloud beta container backup-restore backup-plans create $BACKUP_PLAN \
--project=$PROJECT_ID \
--location=$REGION \
--cluster=projects/${PROJECT_ID}/locations/${ZONE}/clusters/lab-cluster \
--all-namespaces \
--include-secrets \
--include-volume-data \
--cron-schedule="10 3 * * *" \
--backup-retain-days=30
```

You will see the following when it is complete:

![gke plan result](../static/images/gke_backup_00.png)

2. Verify the backup plan was created:

```
gcloud beta container backup-restore backup-plans list \
--project=$PROJECT_ID \
--location=$REGION
```

You will see the following verification:

![gke plan verification](../static/images/gke_backup_01.png)

3. View details about the backup plan:

```
gcloud beta container backup-restore backup-plans describe $BACKUP_PLAN \
--project=$PROJECT_ID \
--location=$REGION
```


![gke plan view details](../static/images/gke_backup_02.png)

## Task 3. Deploy WordPress with MySQL to the cluster


1. Get credentials for `lab-cluster`:

```
gcloud container clusters get-credentials lab-cluster \
--zone=$ZONE
```

You credentials should look like the following:

![get credentials](../static/images/gke_backup_03.png "get credentials")

2. Ensure the reserved static IP address for the application is set:

`echo "EXTERNAL_ADDRESS=${EXTERNAL_ADDRESS}"`

The output should look like this:

![get credentials](../static/images/gke_backup_04.png "get credentials")

## Task 4. Deploy the application

You are now ready to deploy a stateful application. You will deploy the
WordPress application using MySQL as the database.

1. Run the following commands create persistent volumes for the application and
database. The service will also be exposed through a Google Cloud external load
balancer:

```
# Password for lab only. Change to a strong one in your environment.
YOUR_SECRET_PASSWORD=1234567890
kubectl create secret generic mysql-pass --from-literal=password=${YOUR_SECRET_PASSWORD?}
kubectl apply -f https://k8s.io/examples/application/wordpress/mysql-deployment.yaml
kubectl apply -f https://k8s.io/examples/application/wordpress/wordpress-deployment.yaml
```

Your result should look like the following:

![deploy app](../static/images/gke_backup_05.png "deploying app")

2. Patch the service to use EXTERNAL_ADDRESS:

```
patch_file=/tmp/loadbalancer-patch.yaml
cat <<EOF > ${patch_file}
spec:
  loadBalancerIP: ${EXTERNAL_ADDRESS}
  EOF
  kubectl patch service/wordpress --patch "$(cat ${patch_file})"
```

3. Wait for the application to be accessible:

```
while ! curl --fail --max-time 5 --output /dev/null --show-error --silent http://${EXTERNAL_ADDRESS}; do
  sleep 5
  done
  echo -e "\nhttp://${EXTERNAL_ADDRESS} is accessible\n"
```

When the application is accesible you should see the following output:

![deploy app output](../static/images/gke_backup_06.png "deploying app output")

## Task 5. Verify the deployed workload

1. In the Cloud console, navigate to Kubernetes Engine > Workload. You should
see the WordPress application and its database.

![verify on google console](../static/images/gke_backup_07.png "verifying 1")

2. Open a browser window and paste in the URL from the previous step. You
should see the following page:

![verify app](../static/images/gke_backup_08.png "accesing app 1")

3. Click the Continue button and type in the required info. For example:

![verify app](../static/images/gke_backup_09.png "accesing app 2")

4. Make a note of the password and click the Install WordPress button.

After you log in to the WordPress application, try to create some new posts and
add a few comments to existing posts. After backup/restore, you want to verify
your input still exists.

## Task 6. Create a backup

1. Create a backup based on the backup plan:

```
gcloud beta container backup-restore backups create my-backup1 \
--project=$PROJECT_ID \
--location=$REGION \
--backup-plan=$BACKUP_PLAN \
--wait-for-completion
```

Your results should look like this:

![create backup](../static/images/gke_backup_10.png "create backup")

2. View backups:

```
gcloud beta container backup-restore backups list \
--project=$PROJECT_ID \
--location=$REGION \
--backup-plan=$BACKUP_PLAN
```

Your backups should look like this:

![list backups](../static/images/gke_backup_11.png "list backups")

3. View the details of the backup:

```
gcloud beta container backup-restore backups describe my-backup1 \
--project=$PROJECT_ID \
--location=$REGION \
--backup-plan=$BACKUP_PLAN
```

Details of the backup should be similar to this:

![describe backup](../static/images/gke_backup_12.png "describe backups")

## Task 7. Delete the application

You can restore the backup on the same cluster or a different one. In this lab,
you will perform a restore on the same cluster. B.

1. Delete the running application:

```
kubectl delete secret mysql-pass
kubectl delete -f https://k8s.io/examples/application/wordpress/mysql-deployment.yaml
kubectl delete -f https://k8s.io/examples/application/wordpress/wordpress-deployment.yaml
```

When the application are deleted you will see the following:

![destroy app](../static/images/gke_backup_13.png "delate app")

2. Verify the workload is deleted from the [GKE workload page](https://console.cloud.google.com/kubernetes/workload/).

Or from CloudShell by running the following:

`kubectl get pods`

You will see that nothing is found:

`No resources found in default namespace.`

3. Verify you cannot access the application:

`echo -e "\nWordPress URL: http://${EXTERNAL_ADDRESS}\n"`

Click on the URL and verify it is not functional.

## Task 8. Plan a restore

1. Create a restore plan:

```
gcloud beta container backup-restore restore-plans create my-restore-plan1 \
--project=$PROJECT_ID \
--location=$REGION \
--backup-plan=projects/${PROJECT_ID}/locations/${REGION}/backupPlans/$BACKUP_PLAN \
--cluster=projects/${PROJECT_ID}/locations/${ZONE}/clusters/lab-cluster \
--namespaced-resource-restore-mode=delete-and-restore \
--volume-data-restore-policy=restore-volume-data-from-backup \
--all-namespaces
```

You output should look like the following:

![restore output](../static/images/gke_backup_14.png "restore output")

2. View the restore plans:

```
gcloud beta container backup-restore restore-plans list \
--project=$PROJECT_ID \
--location=$REGION
```

3. View the details of restore plan:

```
gcloud beta container backup-restore restore-plans describe my-restore-plan1 \
--project=$PROJECT_ID \
--location=$REGION
```

Your restore plan should look like the following:

![describe plan](../static/images/gke_backup_15.png "describe plan")

## Task 9. Restore a backup

1. Restore the backup:

```
gcloud beta container backup-restore restores create my-restore1 \
--project=$PROJECT_ID \
--location=$REGION \
--restore-plan=my-restore-plan1 \
--backup=projects/${PROJECT_ID}/locations/${REGION}/backupPlans/${BACKUP_PLAN}/backups/my-backup1 \
--wait-for-completion
```

Your progress should look like this:

![restore progress](../static/images/gke_backup_16.png "restore progress")

2. Verify the application is running:

`kubectl get pods`

3. Wait until the all pods have a `STATUS` of `RUNNING`

![pod status](../static/images/gke_backup_16.png "pod status")

4. Verify you can access the application:

`echo -e "\nWordPress URL: http://${EXTERNAL_ADDRESS}\n"`

5. Click on the URL and verify the application is functional.

@jorgemustaine 2023

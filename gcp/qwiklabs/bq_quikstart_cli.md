# BigQuery QwikStart
### With CLI

**for help about one command:**

`bq help query`

**examine the schema of the Shakespeare table in the samples dataset:**

`bq show bigquery-public-data:samples.shakespeare`

**run a query:**

~~~
bq query --use_legacy_sql=false \
'SELECT word, SUM(word_count) AS count
    FROM `bigquery-public-data`.samples.shakespeare
    WHERE word LIKE "%raisin%"
    GROUP BY word'
~~~

**list any existing datasets in your project:**

`bq ls`

**create a new dataset:**

`bq mk babynames`

**creates or updates a table and loads data in a single step:**

`bq load babynames.names2010 yob2010.txt name:string,gender:string,count:integer`

**to see the schema:**

`bq show babynames.names2010`

**run queries:**

`bq query "SELECT name,count FROM babynames.names2010 WHERE gender = 'F' ORDER BY count DESC LIMIT 5"`

**remove datasets:**

`bq rm -r babynames`

# GCP (Google Cloud Platform)

Google is a cloud provider and GCP It's the platform of a lot of services for
Technologies infrastructure and compute. These notes are about the udemy's
course.

## core services in GCP

![GCP core](static/images/gcp_management_tools_00.png "GCP core")

## Intro summary

![Intro Summary](static/images/gcp_intro_summary_00.png)

## Intro summary

![hierarchical organization](static/images/gcp_herarchical_organization_00.png)

# GCP BigQuery

It's a very powerful service used by enterprise customers to analyze data.
Highly scalable enterprise data warehouse. You can use *Bigquery* to examine Cloud
Billing records.

![BigQuery](static/images/bigquery_00.png "BigQuery")

* In BigQuery you can use SQL sentences to request about the fields of
determinate table. To reference a table in a query, you specify the dataset and
table, the project It's optional.

### Some example of Queries in GCP bq gui

* Find the Google Cloud project with the most records.

~~~
SELECT project.id, count(*) as count from `billing_dataset.enterprise_billing` GROUP BY project.id
~~~

*  Find the cost breakdown for each project

~~~
SELECT ROUND(SUM(cost),2) as Cost, project.name from `billing_dataset.enterprise_billing` GROUP BY project.name
~~~

### DevOps tools in GCP

Cloud Services Repositories CSV by google, Cloud Build is CI engine,
Container Registry, IDE integrations with intelliJ and others

### interesting links:

* [gcloud install](https://cloud.google.com/sdk/docs/install)
* [gcloud command sheet](https://cloud.google.com/sdk/docs/cheatsheet)
* [recipe to export data bq related with billing project](https://cloud.google.com/billing/docs/how-to/export-data-bigquery-setup)

@jorgemustaine 2021

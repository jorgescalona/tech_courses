# Create deployment

`kubectl create -f deployments/auth.yaml`

you can verify cluster elements with:

~~~
kubectl get replicasets
kubectl get pods
~~~

time to create a service for our auth deployment:

`kubectl create -f services/auth.yaml`

create and expose the hello Deployment:

~~~
kubectl create -f deployments/hello.yaml
kubectl create -f services/hello.yaml
~~~

create and expose the frontend Deployment:

~~~
kubectl create secret generic tls-certs --from-file tls/
kubectl create configmap nginx-frontend-conf --from-file=nginx/frontend.conf
kubectl create -f deployments/frontend.yaml
kubectl create -f services/frontend.yaml
~~~

**Note:** You created a ConfigMap for the frontend.

Afterwards you can scale your deployment:

`kubectl scale deployment hello --replicas=5`

Verifiying number of pods on the cluster:

`kubectl get pods | grep hello- | wc -l`i

### Interesting links:

* [Setup and Configure a Cloud Environment In GCP: Challange Lab](https://github.com/jahanvisharma-dotcom/Set-up-and-Configure-Google-Cloud-Challenge-Lab/blob/main/Set%20up%20and%20Configure%20a%20Cloud%20Environment%20in%20Google%20Cloud:%20Challenge%20Lab)

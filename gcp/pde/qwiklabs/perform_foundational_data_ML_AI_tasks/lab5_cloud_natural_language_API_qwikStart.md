# Cloud Natural Language API: Qwik Start

## Overview

Cloud Natural Language API lets you extract information about people, places, events, (and more) mentioned in text documents, news articles, or blog posts. You can use it to understand sentiment about your product on social media, or parse intent from customer conversations happening in a call center or a messaging app. You can even upload text documents for analysis.

## Cloud Natural Language API features

**Syntax Analysis:** Extract tokens and sentences, identify parts of speech (PoS) and create dependency parse trees for each sentence.

**Entity Recognition:** Identify entities and label by types such as person, organization, location, events, products and media.

**Sentiment Analysis:** Understand the overall sentiment expressed in a block of text.

**Content Classification:** Classify documents in predefined 700+ categories.

**Multi-Language:** Enables you to easily analyze text in multiple languages including English, Spanish, Japanese, Chinese (Simplified and Traditional), French, German, Italian, Korean and Portuguese.

**Integrated REST API:** Access via REST API. Text can be uploaded in the request or integrated with Cloud Storage.

In this lab you'll use the analyze-entities method to ask the Cloud Natural Language API to extract "entities" (e.g. people, places, and events) from a snippet of text.

## Create an API Key

First, you will set an environment variable with your PROJECT_ID which you will use throughout this codelab:

`export GOOGLE_CLOUD_PROJECT=$(gcloud config get-value core/project)`

Next, create a new service account to access the Natural Language API:

```
gcloud iam service-accounts create my-natlang-sa \
  --display-name "my natural language service account"
```

Then, create credentials to log in as your new service account. Create these credentials and save it as a JSON file "~/key.json" by using the following command:

```
gcloud iam service-accounts keys create ~/key.json \
  --iam-account my-natlang-sa@${GOOGLE_CLOUD_PROJECT}.iam.gserviceaccount.com
```

Finally, set the GOOGLE_APPLICATION_CREDENTIALS environment variable. The environment variable should be set to the full path of the credentials JSON file you created, which you can see in the output from the previous command:

`export GOOGLE_APPLICATION_CREDENTIALS="/home/USER/key.json"`


# Perform Foundational Data, ML, and AI Tasks

## Objetives

* Create a simple Dataproc job
* Create a simple DataFlow job
* Create a simple Dataprep job
* Perform one of the three Google machine learning backed API tasks

## Challenge scenario

As a junior data engineer in Jooli Inc. and recently trained with Google Cloud and a number of data services you have been asked to demonstrate your newly learned skills. The team has asked you to complete the following tasks.

You are expected to have the skills and knowledge for these tasks so don’t expect step-by-step guides.

## Task 1: Run a simple Dataflow job

You have used Dataflow in the quest to load data into BigQuery from Pub/Sub, now use the Dataflow batch template Text Files on Cloud Storage to BigQuery under "Process Data in Bulk (batch)" to transfer data from a Cloud Storage bucket (gs://cloud-training/gsp323/lab.csv). The following table has the values you need to correctly configure the Dataflow job.

You will need to make sure you have:

* Create a BigQuery dataset called `BigQuery Dataset Name` .
* Create a Cloud Storage Bucket called `Cloud Storage Bucket Name` .


|Field |Value |
|------|------|
|JavaScript UDF path in Cloud Storage|`gs://cloud-training/gsp323/lab.js`|
|JSON path|`gs://cloud-training/gsp323/lab.schema`|
|JavaScript UDF name|`transform`|
|BigQuery output table|`Output Table Name`|
|Cloud Storage input path|`gs://cloud-training/gsp323/lab.csv`|
|Temporary BigQuery directory|`Temporary BigQuery Directory`|
|Temporary location|`Temporary Location`|


# Serverless Data Analysis with Dataflow: Side Inputs (Python)


### Overview

In this lab, you learn how to load data into BigQuery and run complex queries. Next, you will execute a Dataflow pipeline that can carry out Map and Reduce operations, use side inputs and stream into BigQuery

### Objective

In this lab, you learn how to use BigQuery as a data source into Dataflow, and how to use the results of a pipeline as a side input to another pipeline.

* Read data from BigQuery into Dataflow

*  Use the output of a pipeline as a side-input to another pipeline

### Download Code Repository

`git clone https://github.com/GoogleCloudPlatform/training-data-analyst`


### Interesting links:

* [ExampleRepo](https://github.com/GoogleCloudPlatform/training-data-analyst "ExampleRepo") 
* [PipelineExample](https://github.com/GoogleCloudPlatform/training-data-analyst/blob/master/courses/data_analysis/lab2/python/JavaProjectsThatNeedHelp.py "pipeline example")


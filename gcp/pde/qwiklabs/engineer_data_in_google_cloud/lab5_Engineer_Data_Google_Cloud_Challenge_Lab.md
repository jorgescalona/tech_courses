# Engineer Data in Google Cloud: Challenge Lab

## Topics tested:
* Create a new BigQuery table from existing data

* Clean data for ML Model using BigQuery, Dataprep or Dataflow

* Build and tune a model in BQML

* Perform a batch prediction into a new table with BQML

### interesting links:

* [Cahllenge github recipe](https://github.com/codewithgarry/Engineer-Data-in-Google-Cloud-Challenge-Lab/blob/master/New%20Text%20Document.txt)
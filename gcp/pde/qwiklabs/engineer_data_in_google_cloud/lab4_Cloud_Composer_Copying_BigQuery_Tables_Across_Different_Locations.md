# Cloud Composer: Copying BigQuery Tables Across Different Locations


In this advanced lab, you will learn how to create and run an [Apache Airflow](https://airflow.apache.org/) workflow in Cloud Composer that completes the following tasks:

* Reads from a config file the list of tables to copy
* Exports the list of tables from a [BigQuery](https://cloud.google.com/bigquery/docs/) dataset located in US to [Cloud Storage](https://cloud.google.com/storage/docs/)
* Copies the exported tables from US to EU Cloud Storage buckets
* Imports the list of tables into the target BigQuery Dataset in EU

![bq tables across locations](../../static/images/lab4_airflow_00.png "bq tables across locations")

## Airflow and core concepts, a brief introduction

While your environment is building, read about the sample file you'll be using in this lab.

[Airflow](https://airflow.apache.org/) is a platform to programmatically author, schedule and monitor workflows.

Use airflow to author workflows as directed acyclic graphs (DAGs) of tasks. The airflow scheduler executes your tasks on an array of workers while following the specified dependencies.

### Core concepts

[DAG](https://airflow.apache.org/docs/apache-airflow/1.10.15/concepts.html) - A Directed Acyclic Graph is a collection of tasks, organized to reflect their relationships and dependencies.

[Operator](https://airflow.apache.org/docs/apache-airflow/1.10.15/concepts.html#operators) - The description of a single task, it is usually atomic. For example, the BashOperator is used to execute bash command.

[Task](https://airflow.apache.org/docs/apache-airflow/1.10.15/concepts.html#tasks) - A parameterised instance of an Operator; a node in the DAG.

[Task Instance](https://airflow.apache.org/docs/apache-airflow/1.10.15/concepts.html#task-instances) - A specific run of a task; characterized as: a DAG, a Task, and a point in time. It has an indicative state: running, success, failed, skipped, ...

The rest of the Airflow concepts can be found [here](https://airflow.apache.org/docs/apache-airflow/1.10.15/concepts.html).



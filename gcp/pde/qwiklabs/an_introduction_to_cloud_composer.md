# An Introduction to Cloud Composer 2.5

[Airflow](https://airflow.apache.org/ "airflow") is a platform to programmatically author, schedule and monitor workflows.

Use Airflow to author workflows as directed acyclic graphs (DAGs) of tasks. The airflow scheduler executes your tasks on an array of workers while following the specified dependencies.

### Core concepts

[DAG](https://airflow.apache.org/docs/apache-airflow/stable/concepts/dags.html "dag")

A Directed Acyclic Graph is a collection of all the tasks you want to run, organized in a way that reflects their relationships and dependencies.

[Operator](https://airflow.apache.org/docs/apache-airflow/stable/concepts/operators.html "operator")

The description of a single task, it is usually atomic. For example, the BashOperator is used to execute bash command.

[Task](https://airflow.apache.org/docs/apache-airflow/stable/concepts/tasks.html "task")

A parameterised instance of an Operator; a node in the DAG.

[Task Instance](https://airflow.apache.org/docs/apache-airflow/stable/concepts/tasks.html#task-instances "task instance")

A specific run of a task; characterized as: a DAG, a Task, and a point in time. It has an indicative state: running, success, failed, skipped, ...

You can read more about the concepts [here](https://airflow.apache.org/docs/apache-airflow/stable/concepts/index.html).


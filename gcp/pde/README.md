<img src="../static/images/professional_data_engineer_badge.jpeg" alt="Professional data engineer badge" width="70"/>

# GCP Professional Data Engineer

Designing and managing solutions using Google Cloud. Through Partner Certification Kickstart, you’ll develop a thorough understanding of Google Cloud solutions to design data processing systems, ensure solution quality and learn how to operationalize machine learning models and data processing systems

### suggested lectures:

* [Learning path Machine Learning on GCP](https://cloud.google.com/training/machinelearning-ai)
* [Benefits and key features of VertexAI](https://cloud.google.com/vertex-ai#section-1)
* [Best practicies implementing ML on GCP]()
* [Google AutoML Cutting throught the HYype](https://www.fast.ai/2018/07/23/auto-ml-3/)
* [Overview Custom containers](https://cloud.google.com/vertex-ai/docs/training/containers-overview?_ga=2.143882930.-601714452.1627921693)
* [Deep Learning Containers](https://cloud.google.com/deep-learning-containers/docs/overview#pre-installed_software)
* []()

### interesting links:

* [Course catalog access](https://partner.cloudskillsboost.google/course_templates/3)
* [practice and examples with schema tabels and explore metadata on BigQuery](https://github.com/GoogleCloudPlatform/training-data-analyst/blob/master/courses/data-engineering/demos/information_schema.md "examples and practice metadata on bq datasets")

@jorgemustaine 2022
# Orchestation automation

***"La tecnología está cambiando a un ritmo cada vez mayor. Los humanos ya no pueden
mantener efectivamente la administración de tecnología sin aprovechar la
automatización. La automatización es la clave para sobrevivir en el mundo
tecnológico actual."***

Esa es la introducción al curso de Orquestación y automatización de la plataforma
[PluralSigth](https://www.pluralsight.com), y este espacio un lugar para las notas
generales producto de la sistematización del mismo.

@jorgemustaine 2019
